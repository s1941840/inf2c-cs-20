#=========================================================================
# Book Cipher Decryption
#=========================================================================
# Decrypts a given encrypted text with a given book.
# 
# Inf2C Computer Systems
# 
# Dmitrii Ustiugov
# 9 Oct 2020
# 
#
#=========================================================================
# DATA SEGMENT
#=========================================================================
.data
#-------------------------------------------------------------------------
# Constant strings
#-------------------------------------------------------------------------

input_text_file_name:         .asciiz  "input_book_cipher.txt"
book_file_name:               .asciiz  "book.txt"
newline:                      .asciiz  "\n"
        
#-------------------------------------------------------------------------
# Global variables in memory
#-------------------------------------------------------------------------
# 
input_text:                   .space 10001       # Maximum size of input_text_file + NULL
.align 4                                         # The next field will be aligned
book:                         .space 10001       # Maximum size of book_file + NULL
.align 4                                         # The next field will be aligned

# You can add your data here!

#=========================================================================
# TEXT SEGMENT  
#=========================================================================
.text

#-------------------------------------------------------------------------
# MAIN code block
#-------------------------------------------------------------------------

.globl main                     # Declare main label to be globally visible.
                                # Needed for correct operation with MARS
main:
#-------------------------------------------------------------------------
# Reading file block. DO NOT MODIFY THIS BLOCK
#-------------------------------------------------------------------------

# opening file for reading (text)

        li   $v0, 13                    # system call for open file
        la   $a0, input_text_file_name  # input_text file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP:                              # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # input_text[idx] = c_input
        la   $a1, input_text($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(input_text_file);
        blez $v0, END_LOOP              # if(feof(input_text_file)) { break }
        lb   $t1, input_text($t0)          
        beq  $t1, $0,  END_LOOP        # if(c_input == '\0')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP
END_LOOP:
        sb   $0,  input_text($t0)       # input_text[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(input_text_file)


# opening file for reading (book)

        li   $v0, 13                    # system call for open file
        la   $a0, book_file_name        # book file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP1:                             # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # book[idx] = c_input
        la   $a1, book($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(book_file);
        blez $v0, END_LOOP1             # if(feof(book_file)) { break }
        lb   $t1, book($t0)          
        beq  $t1, $0,  END_LOOP1        # if(c_input == '\0')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP1
END_LOOP1:
        sb   $0,  book($t0)             # book[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(book_file)

#------------------------------------------------------------------
# End of reading file block.
#------------------------------------------------------------------
     move $a1, $0  #key counter   
     move $a2, $0  # book counter
     move $k0, $0
     addi $k0, $k0, 10 #sets constant value for 10
     #t0 stores key value 1 
     #t1 stores key value 2 
     #t7 holds temp for two digits 
     #s0 holds book char
     #s1 holds x pos 
     #s2 holds y pos
     move $t8, $0
     move $s5, $0
     addi $s5, $s5, -1
MAIN: 
     #handling of first key number
     lb $t0, input_text($a1) 
     beq $t0, $0, END
     addi $t0, $t0, -48
     addi $s5, $s5, 1
     j TwoDigit0
RETURN0:    
     addi $a1, $a1, 2   
     #handling of second key number
     lb $t1, input_text($a1) 
     addi $t1, $t1, -48
     j TwoDigit1
RETURN1:      
     addi $a1, $a1, 2
     
     move $s1, $0 
     addi $s1, $s1, 1
     move $s2, $0 
     addi $s2, $s2, 1
     move $a2, $0
     move $t9, $0
     move $s6, $0
     j SEARCH
SEARCH:    
     lb $s0, book($a2)
     beq $s0, 32, SPACE
Space_Ret:
     beq $s0, 10, LINE
Line_Ret: 
     beq $t0, $s1, PRINT
Print_Ret:
     move $k1,$0
     sub $k1, $t0, $s1
     bltz $k1, NEWLINE   
     addi $a2, $a2, 1
     j SEARCH
NEWLINE: 
     bne $t9, $0, NEWSPACE
     li $v0, 11 
     la $a0, 10
     syscall 
     addi $t8, $t8, 1 
     j MAIN 
NEWSPACE: 
     move $t8, $0 
     j MAIN
PRINT:
     bne $t1, $s2, Print_Ret 
     beq $s5, $0, RET
     beq $s6, $0, ADDSPACE
RET:
     addi $t9, $t9, 1 
     li $v0, 11 
     la $a0, ($s0) 
     syscall
      
     j Print_Ret
ADDSPACE: 
     
     bne $t8, $0, RET
     li $v0, 11 
     la $a0, 32
     syscall 
     addi $s6, $s6, 1
     
     j RET
SPACE:
     addi $s2, $s2, 1 
     addi $a2, $a2, 1
     
     j SEARCH
LINE: 
     addi $s1, $s1, 1
     move $s2, $0
     addi $s2, $s2, 1
     #addi $a2, $a2, 1
     j Line_Ret
TwoDigit0: 
     addi $a1, $a1, 1    #adds one to check next digit 
     lb $t7, input_text($a1) #loads next digit 
     addi $a1, $a1, -1   #removes counter edition 
     beq $t7, 32, RETURN0 #returns if space next 
     addi $a1, $a1, 1  #adds counter back 
     mult $t0, $k0  # multiplies by 10 
     mflo $t0 #sets t0 to output of mult
     addi $t7, $t7, -48 #adds next character 
     add $t0, $t0, $t7
     j RETURN0
TwoDigit1: #for comments see TwoDigit0
     addi $a1, $a1, 1 
     lb $t7, input_text($a1)
     addi $a1, $a1, -1
     beq $t7, 10, RETURN1
     addi $a1, $a1, 1 
     mult $t1, $k0
     mflo $t1
     addi $t7, $t7, -48
     add $t1, $t1, $t7
     j RETURN1
END: 
     beq $t8, $0, LASTLINE
     j main_end 
LASTLINE: 
    li $v0, 11 
    la $a0, 10
    syscall
#------------------------------------------------------------------
# Exit, DO NOT MODIFY THIS BLOCK
#------------------------------------------------------------------
main_end:      
        li   $v0, 10          # exit()
        syscall

#----------------------------------------------------------------
# END OF CODE
#----------------------------------------------------------------
