#=========================================================================
# XOR Cipher Cracking
#=========================================================================
# Finds the secret key for a given encrypted text with a given hint.
# 
# Inf2C Computer Systems
# 
# Dmitrii Ustiugov
# 9 Oct 2020
# 
#
#=========================================================================
# DATA SEGMENT
#=========================================================================
.data
#-------------------------------------------------------------------------
# Constant strings
#-------------------------------------------------------------------------

input_text_file_name:         .asciiz  "input_xor_crack.txt"
hint_file_name:                .asciiz  "hint.txt"
newline:                      .asciiz  "\n"
        
#-------------------------------------------------------------------------
# Global variables in memory
#-------------------------------------------------------------------------
# 
input_text:                   .space 10001       # Maximum size of input_text_file + NULL
.align 4                                         # The next field will be aligned
hint:                         .space 101         # Maximum size of key_file + NULL
.align 4                                         # The next field will be aligned
gap:                         .asciiz " "
# You can add your data here!

#=========================================================================
# TEXT SEGMENT  
#=========================================================================
.text

#-------------------------------------------------------------------------
# MAIN code block
#-------------------------------------------------------------------------

.globl main                     # Declare main label to be globally visible.
                                # Needed for correct operation with MARS
main:
#-------------------------------------------------------------------------
# Reading file block. DO NOT MODIFY THIS BLOCK
#-------------------------------------------------------------------------

# opening file for reading (text)

        li   $v0, 13                    # system call for open file
        la   $a0, input_text_file_name  # input_text file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP:                              # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # input_text[idx] = c_input
        la   $a1, input_text($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(input_text_file);
        blez $v0, END_LOOP              # if(feof(input_text_file)) { break }
        lb   $t1, input_text($t0)          
        beq  $t1, $0,  END_LOOP        # if(c_input == '\0')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP
END_LOOP:
        sb   $0,  input_text($t0)       # input_text[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(input_text_file)


# opening file for reading (hint)

        li   $v0, 13                    # system call for open file
        la   $a0, hint_file_name        # hint file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP1:                             # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # hint[idx] = c_input
        la   $a1, hint($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(key_file);
        blez $v0, END_LOOP1             # if(feof(key_file)) { break }
        lb   $t1, hint($t0)          
        addi $v0, $0, 10                # newline \n
        beq  $t1, $v0, END_LOOP1        # if(c_input == '\n')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP1
END_LOOP1:
        sb   $0,  hint($t0)             # hint[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(key_file)

#------------------------------------------------------------------
# End of reading file block.
#------------------------------------------------------------------
# You can add your code here!
       
       
       move $t2, $0 #stores byte 
       move $t6, $0 #counter for key byte
       move $a1, $0 
       #string variables 
       move $a2, $0 #key counter variable 
       lb $s6, gap 
       lb $s7, newline
       move $s0, $0
       addi $s0, $s0, -1
       move $t9, $0
       addi $t9, $t9, 255
       move $t8, $0 
KEYLOOP: 
       addi $s0, $s0, 1       
XXOR: 
     lb $s1, input_text($a2)
     beq $s1, $0, END
     addi $a2, $a2, 1 #incremetns 
     beq $s1, $s6, SPACES
     beq $s1, $s7, NEWLINE
     xor $s1, $s1, $s0
     #li $v0, 11
     #la $a0, ($s1)
     #syscall
     move $t6, $0
CHECK:
     lb $s2, hint($a1)
     beq $s2, $0, ENDEND
     beq $s2, $s6, SpaHandle
Ret:    
     bne $s2, $s1, FAIL
     addi $a1, $a1, 1 
     move $t8, $0 #space/newline marker
     j XXOR
SpaHandle: 
     move $s2, $0
     addi $s2, $s2, -1
     j Ret 
FAIL: 
     move $a0, $0
     move $t8, $0
     j XXOR
SPACES:
     #li $v0, 11
     #la $a0, ($s1) 
     #syscall 
     move $t6, $0
     move $s1, $0
     addi $s1, $s1, -1
     j CHECK
NEWLINE:
     #li $v0, 11
     #la $a0, ($s7) 
     #syscall 
     move $t6, $0
     #addi $a2, $a2, 1 #incremetns 
     move $s1, $0
     addi $s1, $s1, -1
     j CHECK
ENDEND: 
     move $s5, $0 
     addi $s5, $s5, 128
     move $s3, $0 
     addi $s3, $s3, -2
Lop:
     addi $s3, $s3, 1
     beq $s3, 7, Lin
     and $s4, $s0, $s5
     srl $s5, $s5, 1 
     beq $s4, $0, ZIRO
     li $v0, 11
     la $a0, 49
     syscall
     j Lop
ZIRO: 
     li $v0, 11 
     la $a0, 48
     syscall
     j Lop
Lin: 
     li $v0, 11
     la $a0, ($s7) 
     syscall
     j main_end
END: 
     #li $v0, 11 
     #la $a0, ($s7) 
     #syscall
     
     #li $v0, 1 
     #la $a0, ($s0)
     #syscall
     #li $v0, 11 
     #la $a0, ($s7) 
     #syscall
     move $a2, $0
     move $a1, $0 
     bne $s0, $t9, KEYLOOP
     li $v0, 1 
     la $a0, -1
     syscall
     li $v0, 11
     la $a0, ($s7) 
     syscall
#------------------------------------------------------------------
# Exit, DO NOT MODIFY THIS BLOCK
#------------------------------------------------------------------
main_end:      
        li   $v0, 10          # exit()
        syscall

#----------------------------------------------------------------
# END OF CODE
#----------------------------------------------------------------
