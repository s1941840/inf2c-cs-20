#=========================================================================
# Steganography
#=========================================================================
# Retrive a secret message from a given text.
# 
# Inf2C Computer Systems
# 
# Dmitrii Ustiugov
# 9 Oct 2020
# 
#
#=========================================================================
# DATA SEGMENT
#=========================================================================
.data
#-------------------------------------------------------------------------
# Constant strings
#-------------------------------------------------------------------------

input_text_file_name:         .asciiz  "input_steg.txt"
newline:                      .asciiz  "\n"
        
#-------------------------------------------------------------------------
# Global variables in memory
#-------------------------------------------------------------------------
# 
gap:                          .asciiz " "
input_text:                   .space 10001       # Maximum size of input_text_file + NULL
.align 4                                         # The next field will be aligned

# You can add your data here!

#=========================================================================
# TEXT SEGMENT  
#=========================================================================
.text

#-------------------------------------------------------------------------
# MAIN code block
#-------------------------------------------------------------------------

.globl main                     # Declare main label to be globally visible.
                                # Needed for correct operation with MARS
main:
#-------------------------------------------------------------------------
# Reading file block. DO NOT MODIFY THIS BLOCK
#-------------------------------------------------------------------------

# opening file for reading

        li   $v0, 13                    # system call for open file
        la   $a0, input_text_file_name  # input_text file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP:                              # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # input_text[idx] = c_input
        la   $a1, input_text($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(input_text_file);
        blez $v0, END_LOOP              # if(feof(input_text_file)) { break }
        lb   $t1, input_text($t0)          
        beq  $t1, $0,  END_LOOP        # if(c_input == '\0')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP
END_LOOP:
        sb   $0,  input_text($t0)       # input_text[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(input_text_file)


#------------------------------------------------------------------
# End of reading file block.
#------------------------------------------------------------------
        #t0 is line iteration, i 
        #t1 is line collum, j  
        #t2 is space itterator, k         
        #s0 is current char 
        #a1 is a space character 
        #a2 is counter variable 
        #a3 newline is /n
        move $a2, $0
        move $t0, $0
        move $t1, $0
        move $t2, $0
        move $t5, $0
        lb $a1, gap
        lb $a3, newline
        move $t6, $0 
LINE_ITERATOR: 
        lb $s0, input_text($a2)     #loads char in
        beq $s0, $0, LOOP_TERMINATE #ends if end of file 
        beq $s0, $a3, I_INCREMENT   #increments i variable 
        beq $s0, $a1, SPACE         #increments space counter 
        beq $t1, $t2, PRINT         #prints char 
        addi $a2, $a2, 1            #increments coutner 
        j  LINE_ITERATOR
SPACE:       
        addi $t2, $t2, 1 
        addi $a2, $a2, 1 
        j LINE_ITERATOR
I_INCREMENT:
       bne $t6, $0, ALREADY_PRINTED
       
       addi $t0, $t0, 1
       addi $t1, $t1, 1  
       move $t2, $0         
       li $v0, 11 
       la $a0, ($a3) 
       syscall
       addi $a2, $a2, 1
       addi $t5, $t5, 1         
       j LINE_ITERATOR
ALREADY_PRINTED:
       addi $t0, $t0, 1
       addi $t1, $t1, 1  
       move $t2, $0 
       addi $a2, $a2, 1
       move $t6, $0
       #li $v0, 11 
       #la $a0, ($a1) 
       #syscall
       j LINE_ITERATOR
PRINT: 
       beq $t6, $0, ADDSPACE
HERE:
       addi $t6, $t6, 1 
       li $v0, 11
       la $a0, ($s0)
       beq $s0, $a3, S
RETURNING:
       syscall
       la $t9, ($s0)
       addi $a2, $a2, 1 
       j LINE_ITERATOR
S: 
       la $s0, ($a3)
       j RETURNING
ADDSPACE: 
       beq $t2, $0, HERE
       bne $t5, $0, HERE
       move $t5, $0
       li $v0, 11 
       la $a0, ($a1) 
       syscall
       j HERE
LOOP_TERMINATE:

   beq $t9, $a3, main_end
   li $v0, 11 
   la $a0, ($a3) 
   syscall
  
#------------------------------------------------------------------
# Exit, DO NOT MODIFY THIS BLOCK
#------------------------------------------------------------------
main_end:      
        li   $v0, 10          # exit()
        syscall

#----------------------------------------------------------------
# END OF CODE
#----------------------------------------------------------------
