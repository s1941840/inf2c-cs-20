/*************************************************************************************|
|   1. YOU ARE NOT ALLOWED TO SHARE/PUBLISH YOUR CODE (e.g., post on piazza or online)|
|   2. Fill memory_hierarchy.c                                                        |
|   3. Do not use any other .c files neither alter mipssim.h or parser.h              |
|   4. Do not include any other library files                                         |
|*************************************************************************************/

#include "mipssim.h"

uint32_t cache_type = 0;
//index bits
int bits_for_byte_offset_constant = 2; //constant byte offset as each block has 4 bytes
int acutal_offset = 4; 
int number_of_words = 0; //number of 32 bit words the cache can store
int number_of_blocks = 0; //thefore the number of blocks is ^over 4 as block size is 16 bytes
int index_bits = 0; //log 2 of ^ to get number of bits

int array_size = 0; //size of variable length array 
int tag_bits = 0; 
int sets =0;
int set_bits = 0;
uint32_t *cache;

void memory_state_init(struct architectural_state *arch_state_ptr) {
    arch_state_ptr->memory = (uint32_t *) malloc(sizeof(uint32_t) * MEMORY_WORD_NUM);
    memset(arch_state_ptr->memory, 0, sizeof(uint32_t) * MEMORY_WORD_NUM);
    if (cache_size == 0) {
        // CACHE DISABLED
        memory_stats_init(arch_state_ptr, 0); // WARNING: we initialize for no cache 0
        
    } else {
        
        // CACHE ENABLED
        //assert(0); /// @students: remove assert and initialize cache
        /// @students: memory_stats_init(arch_state_ptr, X); <-- fill # of tag bits for cache 'X' correctly
        int i = 0;
        int j = 0; 
        switch(cache_type) {
        case CACHE_TYPE_DIRECT: // direct mapped
          //index bits calculations
            number_of_words = cache_size / 4; 
            number_of_blocks = number_of_words / 4;
            index_bits = log(number_of_blocks) / log(2);

            //testing
            //printf("\n cache size %u, number of words storeable %u, number of blocks %u, index_bits %u", cache_size,number_of_words,number_of_blocks,index_bits);
            //fflush(stdout);

            array_size = 7 * number_of_blocks; //7 as 7 variables for each cache line -> valid, tag, index , data * 4 
            //CACHE LAYOUT -> INDEX / TAG / VALID / DATA / DATA / DATA / DATA / INDEX / TAG / VALID / DATA / DATA / DATA / DATA /... ETC
            cache = (uint32_t*)calloc(array_size,sizeof(uint32_t));
            
            tag_bits = 32 - acutal_offset - index_bits;
            
            memory_stats_init(arch_state_ptr, tag_bits);
            //printf("\n%u %u\n",tag_bits,arch_state_ptr->bits_for_cache_tag);
            //fflush(stdout);
            //assert(0);
            //set up index values
            
            for (i = 0; i < array_size; i = i +7) { 
                cache[i] = j;
                j = j + 1;
            }
            break;
        case CACHE_TYPE_FULLY_ASSOC: // fully associative
            memory_stats_init(arch_state_ptr, 28);
            number_of_words = cache_size / 4; 
            number_of_blocks = number_of_words / 4;
            index_bits = log(number_of_blocks) / log(2);
            array_size = 7 * number_of_blocks;
            cache = (uint32_t*)calloc(array_size,sizeof(uint32_t));
            break;
        case CACHE_TYPE_2_WAY: // 2-way associative
            number_of_words = cache_size / 4; 
            number_of_blocks = number_of_words / 4;
            sets = number_of_blocks/2;
            set_bits = log(sets) /log(2);
            index_bits = log(number_of_blocks) / log(2);
            memory_stats_init(arch_state_ptr, 32-4-set_bits);
            array_size = 7 * number_of_blocks;
            cache = (uint32_t*)calloc(array_size,sizeof(uint32_t));
            break;
        }
    }
}

void cache_print() { 
    int i = 0;
    for (i=0;i<array_size;i=i+1) { 
        if (i %7 == 0 ) { 
            printf("\n\n%u | ",i/7 );

        }
        printf("  %u  ",cache[i]);
        fflush(stdout);
    }
   
    return;
}
// returns data on memory[address / 4]
int memory_read(int address){
    arch_state.mem_stats.lw_total++;
    check_address_is_word_aligned(address);
    if (cache_size == 0) {
        // CACHE DISABLED
        return (int) arch_state.memory[address / 4];
    } else {
        
        printf("\nmemory address read: %u %u\n",address/4,arch_state.memory[address/4]);
        printf("\nPC: %u %u\n",arch_state.curr_pipe_regs.pc/4, arch_state.IR_meta.opcode);
        fflush(stdout);
        // CACHE ENABLED
        
        //assert(0); /// @students: Remove assert(0); and implement Memory hierarchy w/ cache
        
        /// @students: your implementation must properly increment: arch_state_ptr->mem_stats.lw_cache_hits
        int mod_address = 0;
        int partial_address = 0;
        int original =0;
        int BO = 0; //used in fullassoc
        int i = 0;
        switch(cache_type) {
        case CACHE_TYPE_DIRECT: 
            // direct mapped
            //CACHE LAYOUT -> INDEX / TAG / VALID / DATA / DATA / DATA / DATA / INDEX / TAG / VALID / DATA / DATA / DATA / DATA /... ETC
            //address = 7;
            original = address;
            address = address /4;
            partial_address = get_piece_of_a_word(address,2,31); //removes byte offset from address 
            //printf("\nfuc k%u %u %u %u\n",original,address,get_piece_of_a_word(address,0,2),get_piece_of_a_word(original,0,2));
            mod_address = partial_address % number_of_blocks;
            printf("address %u,partial address %u, mod adddress %u\n",address,partial_address, mod_address);
            fflush(stdout);
            
            
            //fflush(stdout);
            //cache[7+1] = 0;
            //cache[7+2] =1;
            //cache[7+6] = address;
            
            for (i=0;i<array_size;i=i+7) { //loops cache
                
                if (mod_address == cache[i]) {  //checks if index matches 
                    
                    if (cache[i+2] == 1) {  //checks if valid bit is set
                        
                        if (cache[i+1] == get_piece_of_a_word(address,bits_for_byte_offset_constant + index_bits,31)) {  // checks tag
                            //update cache stats
                            printf("Sucsessful hit,   ");
                            printf("offset: %u   actual memory value: %u    cache value:   %u\n",get_piece_of_a_word(address,0,2),arch_state.memory[address],cache[i + 3 + get_piece_of_a_word(address,0,2)]);
                            fflush(stdout);
                            //cache_print();
                            arch_state.mem_stats.lw_cache_hits++;
                            return cache[i+3 + get_piece_of_a_word(address,0,2)];
                            
                        }
                    }
                        
                }

            } 
            printf("miss\n");
            cache[7*mod_address+1] = get_piece_of_a_word(address,bits_for_byte_offset_constant + index_bits,31);
            cache[7*mod_address+2] = 1;
            //printf("%u %u %u %u %u",original/4,((partial_address << 2) + 0),((partial_address << 2) + 4),((partial_address << 2) + 8),((partial_address << 2) + 12));
            cache[7*mod_address + 3 + 0] = arch_state.memory[((partial_address << 2) + 0) ];
            cache[7*mod_address + 3 + 1] = arch_state.memory[((partial_address << 2) + 1) ];
            cache[7*mod_address + 3 + 2] = arch_state.memory[((partial_address << 2) + 2) ];
            cache[7*mod_address + 3 + 3] = arch_state.memory[((partial_address << 2) + 3)];
            //printf("\n%u,%u,%u,%u,%u,%u,%u,%u,%u\n",(0<<2)+1,((partial_address << 2) + 0) / 4,((partial_address << 2) + 4) / 4,((partial_address << 2) + 8) / 4,((partial_address << 2) + 12) / 4,cache[mod_address+3+0],cache[mod_address+3+1],cache[mod_address+3+2],cache[mod_address+3+3]);
            fflush(stdout);
            printf("index: %u tag: %u valid: %u\n",cache[7*mod_address],cache[7*mod_address+1],cache[7*mod_address+2]);
            //cache_print();
            return (int) arch_state.memory[address];
            break;
        case CACHE_TYPE_FULLY_ASSOC: // fully associative
            address = address / 4;
            partial_address = get_piece_of_a_word(address,2,31);
            BO = get_piece_of_a_word(address,0,2);
            int z =0;
            for (i=0;i<array_size;i=i+7) {
                cache[i+2] = cache[i+2] + 1; 
                if (cache[i]==partial_address) { 
                    if (cache[i+1] == 1){ 
                        arch_state.mem_stats.lw_cache_hits++;
                        cache[i+2] = 0;
                        printf("cache hit\n");
                        
                        return cache[i+3+BO];
                    }
                }
            }
            printf("miss\n");
            int k = 0;
            for (k=0;k<array_size;k=k+7) {
                if (cache[k+1]==0) { 
                    cache[k] = get_piece_of_a_word(address,2,31);
                    cache[k + 1] = 1;
                    cache[k + 2] = 0;
                    cache[k + 3 + 0] = arch_state.memory[((partial_address << 2) + 0) ];
                    cache[k + 3 + 1] = arch_state.memory[((partial_address << 2) + 1) ];
                    cache[k + 3 + 2] = arch_state.memory[((partial_address << 2) + 2) ];
                    cache[k + 3 + 3] = arch_state.memory[((partial_address << 2) + 3) ];
                    //cache_print();
                    return (int) arch_state.memory[address];         
                }
            }

            int l = 0; 
            int minval=cache[2]; 
            int index=0;
            for (l=0;l<array_size;l=l+7) { 
                if (cache[l+2]>minval) { 
                    minval = cache[l+2];
                    index = l;
                }
            }
            cache[index]   = get_piece_of_a_word(address,2,31);
            cache[index+1] = 1;
            for (z=0;z<array_size;z=z+7){
                   cache[z+2] = cache[z+2] + 1;
            }
            cache[index + 2] = 0;
            cache[index + 3 + 0] = arch_state.memory[((partial_address << 2) + 0) ];
            cache[index + 3 + 1] = arch_state.memory[((partial_address << 2) + 1) ];
            cache[index + 3 + 2] = arch_state.memory[((partial_address << 2) + 2) ];
            cache[index + 3 + 3] = arch_state.memory[((partial_address << 2) + 3) ];
            //cache_print();
            return (int) arch_state.memory[address];

            break;
        case CACHE_TYPE_2_WAY: // 2-way associative
            //cache_print();
            address = address / 4;
            BO = get_piece_of_a_word(address,0,2);
            mod_address = get_piece_of_a_word(address,2,2+set_bits);
            partial_address = get_piece_of_a_word(address,2,31);
            printf("%u  %u  %u  %u ",address,BO,mod_address,partial_address);
            for (i=0;i<array_size;i=i+7) { 
                cache[i+2] = cache[i+2] + 1;
            }
            if (cache[mod_address*7*2+1]==1){
                if (cache[mod_address*7*2]==partial_address) { 
                    printf("cache hit");
                    arch_state.mem_stats.lw_cache_hits++;
                    cache[mod_address*7*2+2] = 0;
                    return cache[mod_address*7*2+3+BO];
                }
            }
            if (cache[mod_address*7*2+7+1]==1){
                if (cache[mod_address*7*2+7]==partial_address) { 
                    printf("cache hit");
                    arch_state.mem_stats.lw_cache_hits++;
                    cache[mod_address*7*2+7+2] = 0;
                    return cache[mod_address*7*2+7+3+BO];
                }
            }
            printf("cache miss REPLACEMENT");
            if (cache[mod_address*7*2+1]==0){     
                cache[mod_address*7*2 ] = partial_address;
                cache[mod_address*7*2 + 1] = 1;
                cache[mod_address*7*2 + 2] = 0;
                cache[mod_address*7*2 + 3 + 0] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 0) ];
                cache[mod_address*7*2 + 3 + 1] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 1) ];
                cache[mod_address*7*2 + 3 + 2] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 2) ];
                cache[mod_address*7*2 + 3 + 3] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 3) ];
                return (int) arch_state.memory[address];   
            }
            if (cache[mod_address*7*2+7+1]==0){     
                cache[mod_address*7*2+7] = partial_address;
                cache[mod_address*7*2+7 + 1] = 1;
                cache[mod_address*7*2+7 + 2] = 0;
                cache[mod_address*7*2+7 + 3 + 0] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 0) ];
                cache[mod_address*7*2+7 + 3 + 1] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 1) ];
                cache[mod_address*7*2+7 + 3 + 2] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 2) ];
                cache[mod_address*7*2+7 + 3 + 3] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 3) ];
                return (int) arch_state.memory[address];
            }
            //if full
            if (cache[mod_address*7*2+7+2]< cache[mod_address*7*2+2]) { 
                cache[mod_address*7*2 ] = partial_address;
                cache[mod_address*7*2 + 1] = 1;
                cache[mod_address*7*2 + 2] = 0;
                cache[mod_address*7*2 + 3 + 0] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 0) ];
                cache[mod_address*7*2 + 3 + 1] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 1) ];
                cache[mod_address*7*2 + 3 + 2] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 2) ];
                cache[mod_address*7*2 + 3 + 3] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 3) ];
                return (int) arch_state.memory[address];   
            }
            cache[mod_address*7*2+7] = partial_address;
            cache[mod_address*7*2+7 + 1] = 1;
            cache[mod_address*7*2+7 + 2] = 0;
            cache[mod_address*7*2+7 + 3 + 0] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 0) ];
            cache[mod_address*7*2+7 + 3 + 1] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 1) ];
            cache[mod_address*7*2+7 + 3 + 2] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 2) ];
            cache[mod_address*7*2+7 + 3 + 3] = arch_state.memory[((get_piece_of_a_word(address,2,31) << 2) + 3) ];
            return (int) arch_state.memory[address];
            break;
        }
    }
    return 0;
}





// writes data on memory[address / 4]
void memory_write(int address, int write_data) {
    //printf("\nPC: %u\n",arch_state.curr_pipe_regs.pc/4);
    arch_state.mem_stats.sw_total++;
    check_address_is_word_aligned(address);
    
    if (cache_size == 0) {
        // CACHE DISABLED
        arch_state.memory[address / 4] = (uint32_t) write_data;
    } else {
        // CACHE ENABLED
        /// @students: Remove assert(0); and implement Memory hierarchy w/ cache
        
        /// @students: your implementation must properly increment: arch_state_ptr->mem_stats.sw_cache_hits
        //cache_print();
        int mod_address = 0;
        int partial_address = 0;
        int BO=0;
        int z =0;
        int i = 0;
        int a =0;
        switch(cache_type) {
        case CACHE_TYPE_DIRECT: // direct mapped
           
            address = address /4;
            partial_address = get_piece_of_a_word(address,2,31); //removes byte offset from address 
            
            mod_address = partial_address % number_of_blocks;
            //printf("\naddress %u,partial address %u, mod adddress %u\n",address,partial_address, mod_address);
            
            
            //fflush(stdout);
            //cache[7+1] = 0;
            //cache[7+2] =1;
            //cache[7+6] = address;
            for (i=0;i<array_size;i=i+7) { //loops cache
                
                if (mod_address == cache[i]) {  //checks if index matches 
                    
                    if (cache[i+2] == 1) {  //checks if valid bit is set
                        
                        if (cache[i+1] == get_piece_of_a_word(address,bits_for_byte_offset_constant + index_bits,31)) {  // checks tag
                            //update cache stats
                            printf("\nWrite to cache and ram \n");
                            printf("  %u\n",cache[i + 3 + get_piece_of_a_word(address,0,2)]);
                            fflush(stdout);
                            cache[i+3 + get_piece_of_a_word(address,0,2)] = write_data;
                            arch_state.memory[address] = (uint32_t) write_data;
                            //cache_print();
                            arch_state.mem_stats.sw_cache_hits++;
                            return;
                            
                        }
                    }
                        
                }

            } 
            printf("\nWrite just ram %u\n",write_data);
            fflush(stdout);
            arch_state.memory[address] = (uint32_t) write_data;
        
            break;
        case CACHE_TYPE_FULLY_ASSOC: // fully associative
            address = address /4;
            partial_address = get_piece_of_a_word(address,2,31);
            BO = get_piece_of_a_word(address,0,2);
            for (i=0;i<array_size;i=i+7) {
                if (cache[i]==partial_address) { 
                    if (cache[i+1] == 1){ 
                        arch_state.mem_stats.sw_cache_hits++;
                        for (a=0;a<array_size;a=a+7){
                            cache[i+2] = cache[i+2] + 1;
                        }
                        cache[i+2] = 0;
                        printf("write hit\n");
                        cache[i+3+BO] = write_data;
                        arch_state.memory[address] = (uint32_t) write_data;
                        
                        return;
                    }
                }
            }
            printf("write miss\n");
            arch_state.memory[address] = (uint32_t) write_data;
            return;
            break;
        case CACHE_TYPE_2_WAY: // 2-way associative
            //cache_print();
            address = address / 4;
            BO = get_piece_of_a_word(address,0,2);
            mod_address = get_piece_of_a_word(address,2,2+set_bits);
            partial_address = get_piece_of_a_word(address,2,31);
      
            if (cache[mod_address*7*2+1]==1){
                if (cache[mod_address*7*2]==partial_address) { 
                    printf("write hit");
                    for (i=0;i<array_size;i=i+7) { 
                        cache[i+2]=cache[i+2]+1;
                    }
                    arch_state.mem_stats.sw_cache_hits++;
                    cache[mod_address*7*2+2] = 0;
                    cache[mod_address*7*2 + 3 + BO] = write_data;
                    arch_state.memory[address] = (uint32_t) write_data;
                    
                    return;
                }
            }
             if (cache[mod_address*7*2+7+1]==1){
                if (cache[mod_address*7*2+7]==partial_address) { 
                    printf("write hit");
                    for (i=0;i<array_size;i=i+7) { 
                        cache[i+2]=cache[i+2]+1;
                    }
                    arch_state.mem_stats.sw_cache_hits++;
                    cache[mod_address*7*2+7+2] = 0;
                    cache[mod_address*7*2+7 + 3 + BO] = write_data;
                    arch_state.memory[address] = (uint32_t) write_data;
                    return;
                }
            }
            arch_state.memory[address] = (uint32_t) write_data;
            return;
            break;
        }
    }
}